<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = ['id', '_token'];

    protected $fillable = [
        'name', 'type','start_date','end_date','status'
    ];
}
