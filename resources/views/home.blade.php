@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-lg-8 margin-tb">
        <br>
        <div class="pull-left row justify-content-center">
            <h2>Event Management</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('event.create') }}"> Create New Event</a>
        </div>
        <br>
    </div>
</div>

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

<table class="table table-striped">
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>Type</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Status</th>
        <th width="280px">Action</th>
    </tr>
    <?php $counts = 1 ?>
    @forelse($event as $key => $events)
    <tr>
        <td>{{ $event->firstItem() + $key }}</td>
        <td>{{ $events->name }}</td>
        <td>{{ $events->type }}</td>
        <td>{{ $events->start_date }}</td>
        <td>{{ $events->end_date }}</td>
        @if($events->status == 1)
        <td>Active</td>
        @else
        <td>InActive</td>
        @endif
        <td>
            <form onsubmit="return confirm('Please confirm you want to delete!')" action="{{ route('event.destroy',$events->id) }}" method="POST" class="delete">

                <a class="btn btn-primary" href="{{ route('event.edit',$events->id) }}">Edit</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger" >Delete</button>
            </form>
        </td>
    </tr>
    @empty
    <tr><td colspan="2">No data found</td></tr>
@endforelse
</table>
{!! $event->links() !!}

@endsection
