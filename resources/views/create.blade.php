@extends('layouts.master')

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
<div class="col-xl-6">
    <div class="card spur-card">
        <div class="card-header">
            <div class="spur-card-icon">
                <i class="fas fa-chart-bar"></i>
            </div>
            <div class="spur-card-title"> Create Event </div>
        </div>
        <div class="card-body ">
            <form action="{{ route('event.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="exampleFormControlInput1">Name</label>
                    <input type="text" class="form-control" name="name" id="exampleFormControlInput1" placeholder="Event name">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Type</label>
                    <input type="text" class="form-control" name="type" id="exampleFormControlInput1" placeholder="Event type">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Start Date</label>
                    <input type="date" class="form-control" id="exampleFormControlInput1" name="start_date">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">End Date</label>
                    <input type="date" class="form-control" id="exampleFormControlInput1" name="end_date">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Status</label>
                    <select name="status" class="form-control" id="exampleFormControlInput1">
                        <option value="1">Active</option>
                        <option value="2">InActive</option>
                      </select>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
@endsection
