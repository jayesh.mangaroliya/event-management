<div class="dash-nav dash-nav-dark">
    <header>
        <a href="#!" class="menu-toggle">
            <i class="fas fa-bars"></i>
        </a>
        <a href="#" class="spur-logo"></i> <span>Event Manage</span></a>
    </header>
    <nav class="dash-nav-list">
        <a href="{{ route('event.index')}}" class="dash-nav-item">
            <i class="fas fa-cube"></i> Events </a>

    </nav>
</div>
